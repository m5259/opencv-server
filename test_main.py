
from fastapi.testclient import TestClient
import config

path_data = config.PATH_DATA
name_video = 'test_video.mp4'
name_image = 'test_image.jpg'
name_image2 = 'test_image.jpg'
name_image_wrong = 'test_image.jp'


from main import app
testclient = TestClient(app)


def test_read_main():
    res = testclient.get('')
    assert res.status_code == 200
    assert type(res.json()) == dict

def test_video():

    with open(f"{path_data}/{name_image_wrong}", "rb") as _file:
        res = testclient.post("/lk-track/?test=1", files={"file": (name_image_wrong, _file, "image/jpeg")})
    assert res.status_code == 400

    api_list ['lk-track', 'lk-track-video', \
              'opticalflow-dense', 'opticalflow-dense-video', \
              'opticalflow-dense-flow-video']
    for api_loop in api_list:

        with open(f"{path_data}/{name_video}", "rb") as _file:
            res = testclient.post(f"/{api_loop}/?test=1", files={"file": (f"_{name_video}", _file, "image/jpeg")})
        assert res.status_code == 200

def test_image():

    api_list = ['opticalflow-dense-2images', \
                'opticalflow-dense-2images-image']
    for api_loop in api_list:
        with open(f"{path_data}/{name_image}", "rb") as _file1,\
             open(f"{path_data}/{name_image2}", "rb") as _file2:
            
            res = testclient.post(f"/{api_loop}/?test=1", \
                                  files={"file1": (f"_{name_image}", _file1, "image/jpeg"), \
                                         "file2": (f"_{name_image2}", _file2, "image/jpeg")}
            )

        print(res)
        assert res.status_code == 200

    with open(f"{path_data}/{name_image}", "rb") as _file:
        res = testclient.post(f"/4point-algorithm/?test=1", files={"file": (f"_{name_image}", _file, "image/jpeg")})
    assert res.status_code == 200


if __name__ == "__main__":

    test_read_main()

    test_image()
    
    test_video()
    