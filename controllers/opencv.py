from mediaBase.controllers.media_base import media_prod
# from controllers.functions import 
from logconf import mylogger

from cv2_interface.fourpoint_algorithm import cv2_fourpoint_algorithm
logger = mylogger(__name__)
print('__name__', __name__)

class opencv_controller(media_prod):
    def __init__(self, _config):

        super().__init__(_config)

    def process_anyfiles_fg(self, path_file, bgtask, **kwargs):

        if kwargs['process'] == 'opticalflow':
            path_file1 = path_file[0]
            path_file2 = path_file[1]
            fname1 = os.path.basename(path_file1)
            fname2 = os.path.basename(path_file2)
            if kwargs['process-sub'] == 'opticalflow-dense':
                return self.get_info_2images(path_file1, path_file2, **kwargs)
            
            if kwargs['process-sub'] == 'opticalflow-dense-draw':
                if 'ext' in kwargs:
                    fname_export, uuid_export = utils.get_fname_uuid(fname, ext=kwargs['ext'])
                else:
                    fname_export, uuid_export = utils.get_fname_uuid(fname)

                self.opticalflow_dense_image_interface(path_file1, path_file2, f'{self.path_data}/{fname_export}', **kwargs)

                bgtask.add_task(utils.remove_file, f'{self.path_data}/{fname_export}', **kwargs)

                img = cv2.imread(f'{self.path_data}/{fname_export}')
                _, image_enc = cv2.imencode('.png', img)
                return Response(content = image_enc.tostring(), \
                                media_type = 'image/png', \
                                background=bgtask
                )

    def lk_track_video_interface(self, fpath, fpath_ex, **kwargs):

        test = kwargs['test']
        if kwargs['process'] == 'lk-track':
            flow = lk_track(fpath, **kwargs)
            return {'flow_timeseries': flow}
        elif kwargs['process'] == 'lk-track-video':
            flow = lk_track(fpath, fpath_ex, **kwargs)
        
    def opticalflow_dense_image_interface(self, \
                                          fpath1: str, \
                                          fpath2: str, \
                                          fpath_ex: str, \
                                          **kwargs):
        
        if kwargs['process'] == 'opticalflow-dense':
            flow = opticalflow_dense_image(fpath1, fpath2, **kwargs)
            return {'flow': flow}
        elif kwargs['process'] == 'opticalflow-dense-draw':
            if 'export' in kwargs:
                export = kwargs['export']
                del kwargs['export']
            else:
                export = 'org+flow'
            
            opticalflow_dense_image_draw(fpath1, fpath2, fpath_ex. export, **kwargs)
        else:
            raise NotImplementedError("not implemented")

    def opticalflow_dense_video_interface(self, fpath, fpath_ex, **kwargs):

        if kwargs['process'] == 'opticalflow-dense':
            flow = opticalflow-desnse_sum_video(fpath, **kwargs)
            result = {'flow_timeseries': flow}
            return result
        elif kwargs['process'] == 'opticalflow-dense-video':
            if 'export' in kwargs:
                export = kwargs['export']
                del kwargs['export']
            else:
                export = 'org+flow'

            opticalflow_dense_draw(fpath, fpath_ex. export, **kwargs)
        else:
            raise NotImplementedError("not implemented")


    def get_info_2images(self, fpath1: str, fpath2: str, **kwargs):
        if kwargs['process'] == 'opticalflow=dense':
            return self.opticalflow_dense_image_interface(fpath1, fpath2, None, **kwargs)
        else:
            raise NotImplementedError("not implemented")

    def draw_info2image_2images(self, fpath: str, fpath_dst: str, **kwargs):
        
        if kwargs['process'] == 'opticalflow-dense-draw':
            return 

        else:
            raise NotImplementedError("not implemented")


    def draw_info2image(self, fpath: str, fpath_dst: str, **kwargs):
        
        if kwargs['process'] == 'fourpoint-algorithm':
            return cv2_fourpoint_algorithm(fpath, fpath_dst, **kwargs)
        else:
            raise NotImplementedError("not implemented")

    def draw_info2video(self, fpath, fpath_ex, **kwargs):
        if kwargs['process'] == 'lk-track-video':
            return self.lk_track_video_interface(fpath, fpath_ex, **kwargs)
        elif kwargs['process'] == 'opticalflow-dense-video':
            self.opticalflow_dense_video_interface(fpath, fpath_ex, **kwargs)
            if kwargs['fgbg'] == 'bg':
                client = kwargs['client']
                ret = client.update_iten_key('idData', kwargs['uuid_ex'], 'status', 'created')
        else:
            return None

    def get_info_video(self, fpath, **kwargs):
        if kwargs['process'] == 'lk-track':
            return self.lk_track_video_interface(fpath, None, **kwargs)
        elif kwargs['process'] == 'opticalflow-dense':
            return self.opticalflow_dense_video_interface(fpath, None, **kwargs)
        else:
            return None