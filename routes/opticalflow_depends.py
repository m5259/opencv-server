from typing import List, Optional
import cv2

async def params_opticalflow_dense(pyr_scale: Optional[float] = 0.5, \
                                   levels: Optional[int] = 3, \
                                   winsize: Optional[int] = 15, \
                                   iterations: Optional[int] = 3, \
                                   poly_n: Optional[int] = 5, \
                                   poly_sigma: Optional[int] = 1.2, \
                                   flags: Optional[int] = 0, \
                                   test: Optional[int] = None
):
    ret = {
        '_pyr_scale': pyr_scale, \
        '_levels': levels, \
        '_winsize': winsize, \
        '_iterations': iterations, \
        '_poly_n': poly_n, \
        '_poly_sigma': poly_sigma, \
        '_flags': flags, \
        'test': test, \
    }
    return ret


# max_corners = kwargs['_max_corners'] if '_max_corners' in kwargs else 100
# quality_level = kwargs['_quality_level'] if '_quality_level' in kwargs else 0.3
# min_distance = kwargs['_min_distance'] if '_min_distance' in kwargs else 7.0
# blocksize = kwargs['_blocksize'] if '_blocksize' in kwargs else 7
# win_size = kwargs['_win_size'] if '_win_size' in kwargs else 15
# max_level = kwargs['_max_level'] if '_max_level' in kwargs else 2
# lk_criteria = kwargs['_lk_criteria'] if '_lk_criteria' in kwargs else cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT
# lk_count = kwargs['_lk_count'] if '_lk_count' in kwargs else 30
# epsilon = kwargs['_epsilon'] if '_epsilon' in kwargs else 0.03
async def params_lk_track(max_corners: Optional[int] = 100, \
                          quality_level: Optional[float] = 0.3, \
                          min_distance: Optional[float] = 7.0, \
                          blocksize: Optional[int] = 7, \
                          win_size: Optional[int] = 15, \
                          max_level: Optional[int] = 2, \
                          lk_criteria: Optional[int] = cv2.TERM_CRITERIA_EPS, \
                          _lk_count: Optional[int] = 30, \
                          test: Optional[int] = None
):
    ret = {
        '_max_corners': max_corners, \
        '_quality_level': levels, \
        '_min_distance': min_distance, \
        '_blocksize': blocksize, \
        '_win_size': win_size, \
        '_max_level': max_level, \
        '_lk_criteria': lk_criteria, \
        '_lk_count': lk_count, \
        '_epsilon': epsilon, \
        'test': test, \
    }
    return ret
