
import os, sys
print(sys.path)
from fastapi import APIRouter, File, UploadFile, Header, Depends
from fastapi import BackgroundTasks
from typing import List, Optional

from routes.opticalflow_depends import params_opticalflow_dense, params_lk_track
from controllers.opencv import opencv_controller

# opencv_ctr = opencv_controller(config)
from controllers.ctr import opencv_ctr

router_opticalflow = APIRouter(prefix="")

@router_opticalflow.post('/opticalflow-dense-2images/')
def post_2images(file1: UploadFile = File(...), \
                 file2: UploadFile = File(...), \
                 bgtask: BackgroundTasks = BackgroundTasks(), \
                 params: dict = Depends(params_opticalflow_dense)):
    params['process'] = 'opticalflow-dense'
    return opencv_ctr.post_anyfiles_fg([file1, file2], bgtask, **params)

@router_opticalflow.post('/opticalflow-denese-2images-image/')
def post_2images(file1: UploadFile = File(...), \
                 file2: UploadFile = File(...), \
                 bgtask: BackgroundTasks = BackgroundTasks(), \
                 params: dict = Depends(params_opticalflow_dense)):
    params['process'] = 'opticalflow-dense-draw'
    return opencv_ctr.post_anyfiles_fg([file1, file2], bgtask, **params)

@router_opticalflow.post('/lk-track-video/')
def post_2images(file: UploadFile = File(...), \
                 bgtask: BackgroundTasks = BackgroundTasks(), \
                 params: dict = Depends(params_lk_track)):
    
    params['process'] = 'lk-track-video'
    params['ext'] = None
    return opencv_ctr.post_anyfiles_fg([file1, file2], bgtask, **params)


@router_opticalflow.post('/opticalflow-dense/')
def post_video_opticalflow(file: UploadFile = File(...), \
                           params: dict = Depends(params_opticalflow_dense)):
    
    params['process'] = 'opticalflow-dense'
    # params['ext'] = None
    return opencv_ctr.post_info_video_(file, **params)

@router_opticalflow.post('/opticalflow-dense-video/')
def post_video_opticalflow_video(file: UploadFile = File(...), \
                                 bgtask: BackgroundTasks = BackgroundTasks(), \
                                 params: dict = Depends(params_opticalflow_dense)):
    
    params['process'] = 'opticalflow-dense-video'
    params['export'] = 'org+flow'
    return opencv_ctr.post_info_video_fg(file, bgtask, **params)


@router_opticalflow.post('/opticalflow-dense-flow-video/')
def post_video_opticalflow_video(file: UploadFile = File(...), \
                                 bgtask: BackgroundTasks = BackgroundTasks(), \
                                 params: dict = Depends(params_opticalflow_dense)):
    
    params['process'] = 'opticalflow-dense-video'
    params['export'] = 'flow'
    return opencv_ctr.post_info_video_fg(file, bgtask, **params)

    
